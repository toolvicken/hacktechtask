<?php

namespace App\Http\Controllers;

use App\Jobs\ImportData;
use App\Jobs\Mailer;
use App\Models\Category;
use App\Models\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class CsvController extends Controller
{
    public function index(){
        return view('upload_csv');
    }

    public function upload(Request $request){
        $file = $request->file('csv');

        $ext = $file->getClientOriginalExtension();

        $destinationPath = 'uploads';
        $file_name = time().'.'.$ext;
        $file->move($destinationPath, $file_name);

        ImportData::dispatch($file_name);




        Mailer::dispatch($file_name);


        return 'done';

    }






}
