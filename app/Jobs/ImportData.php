<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Company;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $file = public_path('uploads/'. $this->file_name);
        $content = readCSV($file);
        $this->importData($content);
    }

    public function importData($data){

        foreach ($data as $item){
            // step1: create/get category
            $category = $item['category'];
            $category = Category::firstOrCreate(['name' => $category]);

            // step2: create/get company
            $company = Company::firstOrCreate(['name' => $item['company_name'], 'category_id' => $category->id]);

            // step3: create user
            $user = User::firstOrCreate([
                'first_name'=> $item['first_name'],
                'last_name' => $item['last_name'],
                'company_id' => $company->id,
                'email'=> $item['email']
            ]);
        }
    }
}
