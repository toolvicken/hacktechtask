<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class Mailer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file_name;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file_name)
    {
        return $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = public_path('uploads/' . $this->file_name);
        $content = readCSV($file);

        $this->sendMail($content);


    }

    public function sendMail($data)
    {
        foreach ($data as $item) {

            $validator = Validator::make($item, [
                'email' => 'required|email',
                'company' => 'required',
                'category' => 'required'
            ]);

            $data = array(
                'email' => $item['email'],
                'company' => $item['company_name'],
                'category' => $item['category'],
                'subject' => 'testing email',
                'bodyMessage' => 'Best Regards'
            );

            Mail::send('email', $data, function ($message) use ($data) {
                $message->from('test.web.vicken@gmail.com');
                $message->to($data['email']);
                $message->subject($data['subject']);
            });
        }

    }
}
