<?php

if (! function_exists('readCSV')) {
     function readCSV($csvFile)
    {
        $i = 0;
        $handle = fopen($csvFile, 'r');
        while (($row = fgetcsv($handle, 0)) !== false) {
            if (empty($fields)) {
                $fields = $row;
                continue;
            }
            foreach ($row as $k=>$value) {
                $array[$i][$fields[$k]] = $value;
            }
            $i++;
        }
        fclose($handle);

        return $array;
    }
}
