<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CsvController@index');
Route::post('/upload_csv', 'CsvController@upload');


Route::get('test', function(){

    $users = \App\User::with('company')->get();

    $companies = \App\Models\Company::with('category')->get();

    $categories = \App\Models\Category::with('companies')->get();



    dd($users->toArray(), $companies->toArray(), $categories->toArray());
});

//Route::get('test/mail' , 'CsvController@index');
//
//Route::post('test/mail', 'CsvController@upload');
